//------------------------------------------------
// top.v
// David_Harris@hmc.edu 9 November 2005
// Top level system including MIPS and memories
//------------------------------------------------

module mipstop(input clk, reset, cpuStall,
				input [1:0] currP, num,
				input [31:0] sramWriteData,
				inout readOrWrite,//1 read, 0 Write
				inout [31:0] sramAddress,
				output [2:0] info,
				output [31:0] sramReadData,
				output DONE,
				output [31:0] pc);

  wire [31:0] /*pc,*/ instr, dataadr,writedata, readdata;
  wire  stall, memwrite, memread;
  
  // instantiate processor and memories
  mips mips(clk, reset,
				pc, instr, 
				stall || cpuStall, memwrite, memread, dataadr, writedata, readdata,DONE);
  imem imem(pc[7:2], instr);
  
  cacheUnit cache(clk, reset, num,
						//Processor------
						memwrite, memread,
						dataadr, writedata,
						stall,
						readdata,
						//SRAM-----------
						currP,
						sramWriteData,
						readOrWrite,//1 read, 0 Write
						sramAddress,	//Snooping
						info,
						sramReadData);		
  //dmem dmem(clk, memwrite, dataadr, writedata, readdata);

endmodule
