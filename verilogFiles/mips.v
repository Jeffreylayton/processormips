//--------------------------------------------------------------
// mips.v
// David_Harris@hmc.edu and Sarah_Harris@hmc.edu 23 October 2005
// Single-cycle MIPS processor
//--------------------------------------------------------------

// files needed for simulation:
//  mipsttest.v
//  mipstop.v
//  mipsmem.v
//  mips.v
//  mipsparts.v

// single-cycle MIPS processor
module mips(input         clk, reset,
            output [31:0] pc,//yes I MEM
            input  [31:0] instrF,//yes I MEM
				input 		stallGlobal,
            output reg       MemWriteM, MemtoRegM,//yes D MEM
            output reg[31:0] AluOutM, //yes D MEM
				output reg[31:0] WriteDataM,//yes D MEM
            input  [31:0] ReadDataM,
				output DONE); //yes D MEM
				
  	initial begin
		MemWriteM <= 0;
		AluOutM <= 0;
		WriteDataM <= 0;
	end
	
	//---Fetch---
	
	//---
	//wire [31:0] instrF;
	wire [31:0] PCplus4F;
	//-----------
	
	//----Decode------
	reg [31:0] instrD;
	reg [31:0] PCplus4D;
  	initial begin
		instrD <= 0;
		PCplus4D <= 0;
	end

	wire [31:0] Rd1D;//net from regfile
	wire [31:0] Rd2D;//net from regfile
	//-----
	wire PCSrcD;//-To fetch
	wire [31:0] PCBranchD;//-To fetch
	wire [31:0] PCJumpD;//-To fetch
	wire jump;//-To fetch
	wire BranchD;//Direct into Decode (Control)
	//----
	wire RegWriteD;
	wire MemtoRegD;
	wire MemWriteD;
	wire [3:0] ALUControlD;
	wire ALUSrcD;
	wire RegDstD;
	wire hleD;//------
	wire [31:0] Source1D;
	wire [31:0] Source2D;
	wire [4:0] RsD;
	wire [4:0] RtD;
	wire [4:0] RdD;
	wire [31:0] SignImmD;
	//-----------------
	
	//----Execute------
	reg RegWriteE;
	reg MemtoRegE;
	reg MemWriteE;
	reg [3:0] ALUControlE;
	reg ALUSrcE;
	reg RegDstE;
	reg hleE;//------
	reg [31:0] Source1E;
	reg [31:0] Source2E;
	reg [4:0] RsE;
	reg [4:0] RtE;
	reg [4:0] RdE;
	reg [31:0] SignImmE;
  	initial begin
		RegWriteE <= 0;
		MemtoRegE <= 0;
		MemWriteE <= 0;
		ALUControlE <= 0;
		ALUSrcE <= 0;
		RegDstE <= 0;
		hleE <= 0;
		Source1E <= 0;
		Source2E <= 0;
		RsE <= 0;
		RtE <= 0;
		RdE <= 0;
		SignImmE <= 0;
	end
	//-----
	//wire RegWriteE;
	//wire MemtoRegE;
	//wire MemWriteE;
	//wire hleE;//------
	wire [31:0] AluOutE;
	wire [31:0] hiE;
	wire [31:0] loE;
	wire [31:0] WriteDataE;
	wire [4:0] WriteRegE;
	//-------------------------
	
	//----Memory------
	reg RegWriteM;
	//reg MemtoRegM;//port
	//reg MemWriteM;//port
	reg hleM;//------
	//reg [31:0] AluOutM;//port
	reg [31:0] hiM;
	reg [31:0] loM;
	//reg [31:0] WriteDataM;//port
	reg [4:0] WriteRegM;
		initial begin
			RegWriteM <= 0;
			MemtoRegM <= 0;
			MemWriteM <= 0;
			hleM <= 0;
			AluOutM <= 0;
			hiM <= 0;
			loM <= 0;
			WriteDataM <= 0;
			WriteRegM <= 0;
		end
	//-----
	//wire RegWriteM;
	//wire MemtoRegM;
	//wire hleM;//------
	//wire [31:0] ReadDataM;//port
	//wire [31:0] AluOutM;
	//wire [31:0] hiM;
	//wire [31:0] loM;
	//wire [4:0] WriteRegM;
	//---------------------
	
	//----Write------
	reg RegWriteW;
	reg MemtoRegW;
	reg hleW;//------
	reg [31:0] ReadDataW;
	reg [31:0] AluOutW;
	reg [31:0] hiW;
	reg [31:0] loW;
	reg [4:0] WriteRegW;
		initial begin
			RegWriteW <= 0;
			MemtoRegW <= 0;
			hleW <= 0;
			ReadDataW <= 0;
			AluOutW <= 0;
			hiW <= 0;
			loW <= 0;
			WriteRegW <= 0;
		end
	//-----
	wire [31:0] ResultW;
	//wire [31:0] hiW;
	//wire [31:0] loW;
	//----------------------------------------
		
	

  controller c(instrD[31:26], instrD[5:0],
               MemtoRegD, MemWriteD,
               ALUSrcD, RegDstD, RegWriteD, jump, hleD,
               ALUControlD, BranchD);

  regfile     rf(~clk, RegWriteW,hleW, RsD,
                 RtD, WriteRegW,
                 ResultW, hiW, loW, Rd1D, Rd2D);
	

  wire StallF, StallD, FlushE;
  wire [1:0] ForwardAD, ForwardBD;
  wire [2:0] ForwardAE, ForwardBE;
  HazardUnit hu( 	BranchD,
						RsD, RtD,
						RegWriteE, MemtoRegE, hleE,
						RsE, RtE, WriteRegE,
						RegWriteM, MemtoRegM, hleM,
						WriteRegM,
						RegWriteW, hleW,
						WriteRegW,
						//Output Controls 
						StallF,
						StallD,
						ForwardAD, ForwardBD,
						FlushE,
						ForwardAE, ForwardBE);

//-------------------------------------------------
 datapathf dpf(clk, reset,//Common
					PCSrcD, jump,//Control Inputs
					//I- MEM Access
					pc,
					//-----
					PCBranchD, PCJumpD,//Data Inputs
					PCplus4F,//Data Outputs
					StallF || stallGlobal);//Hazard Inputs
//------ F-D ----
wire enableFD, CLRFD;

assign DONE = &instrF; 

assign CLRFD = PCSrcD || jump;
assign enableFD = ~(StallD || stallGlobal);

always @(posedge clk, posedge reset)begin
	if(reset) begin
		instrD <= 32'h00000021;
		PCplus4D <= 32'b0;
	end
	else if(CLRFD) begin
		instrD <= 32'b0;
		PCplus4D <= 32'b0;
	end
	else if(enableFD) begin
		instrD <= instrF;
		PCplus4D <= PCplus4F;
	end
end

datapathd dpd( clk, reset,//Common
					BranchD, //Control Inputs
					PCSrcD, //Control Outputs
					//Register file Access
						Rd1D, Rd2D,
					//-------------
					instrD, PCplus4D, //Data Inputs
					AluOutM, hiM, loM, //FORWARDED  DATA
					PCBranchD, PCJumpD, SignImmD,Source1D,Source2D,//Data Ouputs
					ForwardAD, ForwardBD);//Hazard Inputs

assign RsD = instrD[25:21];
assign RtD = instrD[20:16];
assign RdD = instrD[15:11];
//------ D-E ----
wire CLRDE;
wire enableDE;
assign CLRDE = FlushE;
assign enableDE = ~stallGlobal;
always @(posedge clk, posedge reset)begin
	if(reset)begin
		RegWriteE <= 1'b0;
		MemtoRegE <= 1'b0;
		MemWriteE <= 1'b0;
		ALUControlE <= 4'b0;
		ALUSrcE <= 1'b0;
		RegDstE <= 1'b0;
		hleE <= 1'b0;//------
		Source1E <= 32'b0;
		Source2E <= 32'b0;
		RsE <= 5'b0;
		RtE <= 5'b0;
		RdE <= 5'b0;
		SignImmE <= 32'b0;
	end
	else if(CLRDE)begin
		RegWriteE <= 1'b0;
		MemtoRegE <= 1'b0;
		MemWriteE <= 1'b0;
		ALUControlE <= 4'b0;
		ALUSrcE <= 1'b0;
		RegDstE <= 1'b0;
		hleE <= 1'b0;//------
		Source1E <= 32'b0;
		Source2E <= 32'b0;
		RsE <= 5'b0;
		RtE <= 5'b0;
		RdE <= 5'b0;
		SignImmE <= 32'b0;
	end
	else if(enableDE) begin
		RegWriteE <= RegWriteD;
		MemtoRegE <= MemtoRegD;
		MemWriteE <= MemWriteD;
		ALUControlE <= ALUControlD;
		ALUSrcE <= ALUSrcD;
		RegDstE <= RegDstD;
		hleE <= hleD;//------
		Source1E <= Source1D;
		Source2E <= Source2D;
		RsE <= RsD;
		RtE <= RtD;
		RdE <= RdD;
		SignImmE <= SignImmD;
	end
end


datapathe dpe(clk, reset,//Common
					RegDstE, ALUSrcE, ALUControlE,//Control Inputs
					Source1E, Source2E, RtE, RdE, SignImmE,//Data Inputs
					AluOutM, hiM, loM, hiW, loW, ResultW, //FORWARDED  DATA
					AluOutE, hiE, loE, WriteDataE,WriteRegE,//Data Ouputs
					ForwardAE, ForwardBE);//Hazard Inputs
wire enableEM;
assign enableEM = ~stallGlobal;
always @(posedge clk, posedge reset)begin
	if(reset)begin
		RegWriteM <= 1'b0;
		MemtoRegM <= 1'b0;
		MemWriteM <= 1'b0;
		hleM <= 1'b0;//------
		AluOutM <= 32'b0;
		hiM <= 32'b0;
		loM <= 32'b0;
		WriteDataM <= 32'b0;
		WriteRegM <= 5'b0;
	end
	else if(enableEM) begin
		RegWriteM <= RegWriteE;
		MemtoRegM <= MemtoRegE;
		MemWriteM <= MemWriteE;
		hleM <= hleE;//------
		AluOutM <= AluOutE;
		hiM <= hiE;
		loM <= loE;
		WriteDataM <= WriteDataE;
		WriteRegM <= WriteRegE;
	end
end

//------ M-W ----
wire enableMW;
assign enableMW = ~stallGlobal;
always @(posedge clk, posedge reset)begin
	if(reset)begin
		RegWriteW <= 1'b0;
		ReadDataW <= 32'b0;
		hleW <= 1'b0;//------
		AluOutW <= 32'b0;
		hiW <= 32'b0;
		loW <= 32'b0;
		WriteRegW <= 5'b0;
		MemtoRegW <= 0;
	end
	else if(enableMW) begin
		RegWriteW <= RegWriteM;
		hleW <= hleM;//------
		MemtoRegW <= MemtoRegM;
		ReadDataW <= ReadDataM;
		AluOutW <= AluOutM;
		hiW <= hiM;
		loW <= loM;
		WriteRegW <= WriteRegM;
	end
end

datapathw dpw(clk, reset,//Common
					MemtoRegW,//Control Inputs
					ReadDataW, AluOutW,//Data Inputs
					ResultW);//Data Ouputs

//-------------------------------------------------

endmodule

module controller(input  [5:0] 	op, funct,
                  output       	memtoreg, memwrite,
                  output       	alusrc,
                  output       	regdst, regwrite,
                  output       	jump,
						output		 	hle,
                  output [3:0] 	alucontrol,
						output 			branch);

  wire [1:0] aluop;
  wire regdstt, regwritet;

  maindec md(op, memtoreg, memwrite, branch,
             alusrc, regdstt, regwritet, jump, 
             aluop);
  aludec  ad(funct, aluop, hle, alucontrol);
  
  assign regdst = (hle) ? 1'b0 : regdstt;
  assign regwrite = (hle) ? 1'b0 : regwritet;
endmodule

module maindec(input  [5:0] op,
               output       memtoreg, memwrite,
               output       branch, alusrc,
               output       regdst, regwrite,
               output       jump,
               output [1:0] aluop);

  reg [8:0] controls;
  
  initial begin
		controls <= 9'b0;
	end

  assign {regwrite, regdst, alusrc,
          branch, memwrite,
          memtoreg, jump, aluop} = controls;

  always @(*)
    case(op)
      6'b000000: controls <= 9'b110000010; //Rtype
      6'b100011: controls <= 9'b101001000; //LW
      6'b101011: controls <= 9'b001010000; //SW
      6'b000100: controls <= 9'b000100001; //BEQ
      6'b001001: controls <= 9'b101000000; //ADDIU
      6'b000010: controls <= 9'b000000100; //J
      default:   controls <= 9'b000000000; //???
    endcase
endmodule

module aludec(input      [5:0] funct,
              input      [1:0] aluop,
				  output					hle,
              output  [3:0] alucontrol);
				  
  reg [4:0] controls;
  assign {hle, alucontrol} = controls;
  
  
  initial begin
		controls <= 5'b0;
	end

  always @(*)
    case(aluop)
      2'b00: controls <= 5'b00010;  // add (non - r)
      2'b01: controls <= 5'b00110;  // sub (non - r)
      default: case(funct)          // RTYPE
			 6'b000100:controls <= 5'b01100; // sllv
			 6'b000110:controls <= 5'b01110; // srlv
			 6'b011000:controls <= 5'b11000; // mult
			 6'b011001:controls <= 5'b11001; // multu
			 6'b011010:controls <= 5'b11010; // div
			 6'b011011:controls <= 5'b11011; // divu
          6'b100001:controls <= 5'b00010; // ADDU
          6'b100011:controls <= 5'b00110; // SUBU
          6'b100100:controls <= 5'b00000; // AND
          6'b100101:controls <= 5'b00001; // OR
          6'b100110:controls <= 5'b00011; // XOR
          6'b100111:controls <= 5'b00100; // NOR
          6'b101010:controls <= 5'b00111; // SLT
			 6'b101011:controls <= 5'b01111; // SLTU
          default: controls <= 5'b0xxxx; // ???
        endcase
    endcase
	 
endmodule
//------------------------------------------------------------------------------------------------

module  datapathf(input 			clk, reset,//Common
						input 			PCSrc, jump,//Control Inputs
						//I- MEM Access
						output[31:0] 	pc, 
						//-----
						input[31:0]		PCBranch, PCJump,//Data Inputs
						output[31:0]	PCplus4,//Data Outputs
						input 			Stall);//Hazard Inputs (Global stall also)

  wire [31:0] PCNext,PCNextBR;

  flopenr #(32) pcreg(clk, reset, ~Stall, PCNext, pc);
  adder       pcadd1(pc, 32'b100, PCplus4);
  mux2 #(32)  pcbrmux(PCplus4, PCBranch, PCSrc,
                      PCNextBR);
  mux2 #(32)  pcmux(PCNextBR, PCJump, 
                    jump, PCNext);

endmodule



module datapathd(	input 				clk, reset,//Common
						input 			 	BranchD, //Control Inputs
						output 				PCSrcD, //Control Outputs
						//Register file Access
						input[31:0]			Rd1D, Rd2D,
						//-------------
						input[31:0]			instrD, PCPlus4D, //Data Inputs
						input[31:0]			ALUOutM, hiM, loM, //FORWARDED  DATA
						output[31:0]		PCBranchD, PCJumpD, SignimmD,
						output reg[31:0] 	Source1D, Source2D,//Data Ouputs
						input[1:0]			ForwardAD, ForwardBD);//Hazard Inputs
	initial begin
		Source1D <= 0;
		Source2D <= 0;
	end

 wire [31:0] signimmsh;
 wire EqualD;
 
 assign PCSrcD = BranchD & EqualD;
 
 //---------
 signext     se(instrD[15:0], SignimmD);
 //BRANCH
 sl2         immsh(SignimmD, signimmsh);
 adder       pcadd2(PCPlus4D, signimmsh, PCBranchD);
 //JUMP
 assign PCJumpD = {PCPlus4D[31:28],instrD[25:0], 2'b00};
 //----------
  always @(*)
    case(ForwardAD)
      2'b00: Source1D <= Rd1D;  
      2'b01: Source1D <= ALUOutM;  
		2'b10: Source1D <= hiM;  
      2'b11: Source1D <= loM;  
    endcase
	 
  always @(*)
    case(ForwardBD)
      2'b00: Source2D <= Rd2D;  
      2'b01: Source2D <= ALUOutM;  
		2'b10: Source2D <= hiM;  
      2'b11: Source2D <= loM;  
    endcase
	 
 assign EqualD = (Source1D == Source2D);


endmodule

					
module datapathe(input         clk, reset, //Common
                input         RegDstE, ALUSrcE, 
					 input [3:0]   ALUControlE,//Control Inputs
                input [31:0]  Source1E, Source2E, 
					 input [4:0]	RtE, RdE, 
					 input [31:0]  SignImmE,//Data Inputs
                input [31:0]  ALUOutM, hiM, loM, hiW, loW, ResultW, //FORWARDED  DATA
                output [31:0] AluOutE, hiE, loE, 
					 output reg [31:0]WriteDataE,
					 output [4:0]  WriteRegE,//Data Ouputs
                input  [2:0]  ForwardAE, ForwardBE);//Hazard Inputs

  wire [4:0]  	writereg;
  reg [31:0] 	srca;
  wire [31:0]	srcb;
  wire zero;
  
  	initial begin
		WriteDataE <= 0;
		srca <= 0;
	end

  mux2 #(5)  destmux(RtE, RdE, RegDstE,
                      WriteRegE);

  // ALU logic
  mux2 #(32)  srcbmux(WriteDataE, SignImmE, ALUSrcE,
                      srcb);
  alu         alu(srca, srcb, ALUControlE,
                  AluOutE, zero, hiE, loE);
						
  always @(*)
    case(ForwardAE)
      3'b000: srca <= Source1E;  
      3'b001: srca <= ResultW;  
		3'b010: srca <= hiW;  
      3'b011: srca <= loW;
      3'b100: srca <= ALUOutM;  
      3'b101: srca <= hiM;  
		3'b110: srca <= loM;  
      3'b111: srca <= 32'bx;  
    endcase
	 
  always @(*)
    case(ForwardBE)
      3'b000: WriteDataE <= Source2E;  
      3'b001: WriteDataE <= ResultW;  
		3'b010: WriteDataE <= hiW;  
      3'b011: WriteDataE <= loW;
      3'b100: WriteDataE <= ALUOutM;  
      3'b101: WriteDataE <= hiM;  
		3'b110: WriteDataE <= loM;  
      3'b111: WriteDataE <= 32'bx;    		
    endcase
endmodule

					
module datapathw(input         clk, reset,//Common
                input         MemtoRegW,//Control Inputs
                input  [31:0] ReadDataW, AluOutW,//Data Inputs
                output [31:0] ResultW);//Data Ouputs

  mux2 #(32)  resmux(AluOutW, ReadDataW,
                     MemtoRegW, ResultW);

endmodule


module HazardUnit(//Input Controls
						input 			BranchD,
						input  [4:0]	RsD, RtD,
						input 			RegWriteE, MemtoRegE, hleE,
						input  [4:0]	RsE, RtE, WriteRegE,
						input 			RegWriteM, MemtoRegM, hleM,
						input  [4:0]	WriteRegM,
						input 			RegWriteW, hleW,
						input  [4:0]	WriteRegW,
						//Output Controls 
						output 			StallF,
						output 		 	StallD,
						output reg [1:0] 	ForwardAD, ForwardBD,
						output 			FlushE,
						output reg [2:0]	ForwardAE, ForwardBE);
	
	initial begin 
		ForwardAD <= 0;
		ForwardBD <= 0;
		ForwardAE <= 0;
		ForwardBE <= 0;
	end
						
	wire lwstall, hleCheck, branchstall, branchstallE, branchstallM;							
							
			
	always @(*)	
		if ((RsE == 5'b01110) && hleM) 
			ForwardAE <= 3'b110;
		else if ((RsE == 5'b01111) && hleM) 
			ForwardAE <= 3'b101;
		else if ((RsE != 0) && (RsE == WriteRegM) && RegWriteM && ~hleM)
			ForwardAE <= 3'b100;
		else if ((RsE == 5'b01110) && hleW) 
			ForwardAE <= 3'b011;
		else if ((RsE == 5'b01111) && hleW) 
			ForwardAE <= 3'b010;
		else if ((RsE != 0) && (RsE == WriteRegW) && RegWriteW && ~hleW) 
			ForwardAE = 3'b001;
		else 
			ForwardAE = 3'b000;
	
	always @(*)	
		if ((RtE == 5'b01110) && hleM) 
			ForwardBE <= 3'b110;
		else if ((RtE == 5'b01111) && hleM) 
			ForwardBE <= 3'b101;
		else if ((RtE != 0) && (RtE == WriteRegM) && RegWriteM && ~hleM)
			ForwardBE <= 3'b100;
		else if ((RtE == 5'b01110) && hleW) 
			ForwardBE <= 3'b011;
		else if ((RtE == 5'b01111) && hleW) 
			ForwardBE = 3'b010;
		else if ((RtE != 0) && (RtE == WriteRegW) && RegWriteW && ~hleW) 
			ForwardBE <= 3'b001;
		else 
			ForwardBE <= 3'b000;

	assign lwstall = (( (RsD == RtE) || (RtD == RtE) ) && MemtoRegE);
	
	
	always @(*)
		if ( (RsD == 5'b01110) && hleM )
			ForwardAD <= 2'b11;
		else if ( (RsD == 5'b01111) && hleM )
			ForwardAD <= 2'b10;
		else if ((RsD != 0) && (RsD == WriteRegM) && RegWriteM && ~hleM)
			ForwardAD <= 2'b01;
		else
			ForwardAD <= 2'b00;
	
	always @(*)
		if ( (RtD == 5'b01110) && hleM )
			ForwardBD <= 2'b11;
		else if ( (RtD == 5'b01111) && hleM )
			ForwardBD <= 2'b10;
		else if ((RtD != 0) && (RtD == WriteRegM) && RegWriteM && ~hleM)
			ForwardBD <= 2'b01;
		else
			ForwardBD <= 2'b00;
			
			
	assign hleCheck = ( 5'b01110 == RsD || 5'b01111 == RsD || 5'b01110 == RtD || 5'b01111 == RtD);
		
	assign branchstallE = ( RegWriteE && (WriteRegE == RsD || WriteRegE == RtD) && ~hleE ) || ( hleE && hleCheck);// 
	
	assign branchstallM = ( MemtoRegM && (WriteRegM == RsD || WriteRegM == RtD) );//Loading to current register
	
	assign branchstall = BranchD && ( branchstallE || branchstallM);
	
	assign StallF = lwstall || branchstall;
	assign StallD = lwstall || branchstall;
	assign FlushE = lwstall || branchstall;
	
endmodule