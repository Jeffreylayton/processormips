//------------------------------------------------
// mipstest.v
// David_Harris@hmc.edu 23 October 2005
// Testbench for MIPS processor
//------------------------------------------------

//module mipstest (
//	input CLOCK_50_B5B,
//	input [3:0] KEY,
//	output [7:0] LEDG,
//	output [7:0] LEDR
//);
//
//wire [31:0] writedata, dataadr;
//wire memwrite;
//
//top(CLOCK_50_B5B, KEY[0], writedata, dataadr, memwrite);
//
//assign LEDG 
//
//endmodule 





module mipstest();

  reg         clk;
  reg         reset;

  wire [31:0] writedata, pc, dataadr;
  wire memwrite;

  // instantiate device to be tested
  top dut(clk, reset, writedata,pc , dataadr, memwrite);
  
  // initialize test
  initial
    begin
      reset <= 1; # 22; reset <= 0;
    end

  // generate clock to sequence tests
  always
    begin
      clk <= 1; # 5; clk <= 0; # 5;
    end
	 


  // check that 7 gets written to address 84
  always@(negedge clk)
    begin
      if(pc > 140) begin
			$stop;
      end
    end
endmodule



