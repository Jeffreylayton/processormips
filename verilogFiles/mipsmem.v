//------------------------------------------------
// mipsmemsingle.v
// David_Harris@hmc.edu 23 October 2005
// External memories used by MIPS single-cycle
// processor
//------------------------------------------------

module dmem(input         clk, we,
            input  [31:0] a, wd,
            output [31:0] rd);

  reg  [31:0] RAM[63:0];
  
  initial
    begin
      $readmemh("memoryFiles/Dmem.dat",RAM);
    end
  

  assign rd = RAM[a[31:2]]; // word aligned

  always @(posedge clk)
    if (we)
      RAM[a[31:2]] <= wd;
endmodule


module imem(input  [5:0] a,
            output [31:0] rd);

  reg  [31:0] RAM[63:0];

  initial
    begin
      $readmemh("memoryFiles/memfile.dat",RAM);
    end

  assign rd = RAM[a]; // word aligned
endmodule

module Two_Way_RAM #(parameter WIDTH = 1, parameter LENGTH = 2, parameter SET_LENGTH = 1)
					(input clk, cacheWE, way, 
					input [SET_LENGTH - 1:0] set,
					input [WIDTH - 1:0] wData, 
					output [WIDTH - 1:0] rData);
					
	reg [WIDTH - 1:0] way0[LENGTH - 1:0]; //250 entires 32 bit data
	reg [WIDTH - 1:0] way1[LENGTH - 1:0]; //250 entires 32 bit data
	
	
	wire [1:0] cont;
	assign cont = {cacheWE, way};
	
	assign rData = way ? way1[set] : way0[set];
	
	always @(posedge clk)
		case (cont) 
			2'b10: way0[set] <= wData; // write to way0
			2'b11: way1[set] <= wData; // write to way1
		endcase	
			
endmodule

module Two_Way_RAM_Two_Way_Read #(parameter WIDTH = 1, parameter LENGTH = 2, parameter SET_LENGTH = 1)
					(input clk, cacheWE, way, 
					input [SET_LENGTH - 1:0] set,
					input [WIDTH - 1:0] wData, 
					output [WIDTH*2 - 1:0] rData);
					
	reg [WIDTH - 1:0] way0[LENGTH - 1:0]; //250 entires 32 bit data
	reg [WIDTH - 1:0] way1[LENGTH - 1:0]; //250 entires 32 bit data
	
	
	wire [1:0] cont;
	assign cont = {cacheWE, way};
	
	assign rData = {way1[set],way0[set]};
	
	always @(posedge clk)
		case (cont) 
			2'b10: way0[set] <= wData; // write to way0
			2'b11: way1[set] <= wData; // write to way1
		endcase	
			
endmodule


module cacheUnit(	input clk, reset, 
						input[1:0] NUM,
						//Processor------
						input procWe, procRe,
						input [31:0] addr, procWriteData,
						output procStall,
						output [31:0] procReadData,
						//SRAM-----------
						input [1:0] currProc,
						input [31:0] sramWriteData,
						inout readOrWrite,//1 read, 0 Write
						inout [31:0] sramAddress,	//Snooping
						output [2:0] info,
						output [31:0] sramReadData);					  
		
	reg [2:0] state;
	reg [2:0] statep;
	wire [6:0] control;				  					  
	reg[31:0] d;

	wire active;
	wire activeWrite;
	wire [23:0] tagActive;
	wire [7:0] setActive;
	wire hit0Active, hit1Active, hitActive, wayActive;	
	
	wire passiveWrite;
	wire [23:0] tagSnoop;
	wire [7:0] setSnoop;
	wire hit0Snoop, hit1Snoop, hitSnoop;	
	
	wire [47:0] rTag;
	wire [1:0] rValid;
	
	wire [31:0] wData;
	wire wValid;
	wire [23:0] wTag;
	
	wire Way;
	wire [7:0] Set;
//------------------------------------------------------------	
	initial begin
		state <= 3'b100;
		statep <= 3'b100;
		d <= 32'hA521C90A;
	end
	always @(posedge clk or posedge reset) begin
		if(reset) begin
			state <= 3'b100;
		end
		else begin 
			state <= statep;
			d <= {d[30:0], d[31] ^ d[28]}; //use d[0]
		end
	end
	
//--------------------------------------------------------------	
	assign tagActive 	= addr[31:8]; // assuming input length is 32 bits 
	assign setActive 	= addr[7:0]; // assuming input length is 32 bits
	assign hit0Active = (tagActive == rTag[23:0]) && rValid[0]; 
	assign hit1Active = (tagActive == rTag[47:24]) && rValid[1];
	assign hitActive 	= hit0Active || hit1Active;
	assign wayActive 	= (hitActive)? hit1Active : d[0]; // if theres a miss, use random way else use way that already contains data	
//------------Snooping------------------------------------------	
	assign tagSnoop = sramAddress[31:8]; // assuming input length is 32 bits 
	assign setSnoop = sramAddress[7:0]; // assuming input length is 32 bits
	assign hit0Snoop = (tagSnoop == rTag[0]);
	assign hit1Snoop = (tagSnoop == rTag[1]);
	assign hitSnoop = hit0Snoop || hit1Snoop;
//-------------------------------------------------------------
	assign Way 		= 	(active) ? wayActive : hit1Snoop;
	assign Set 		= 	(active) ? setActive : setSnoop;
	assign wValid	= 	active;
	assign wTag 	= 	(active) ? tagActive : tagSnoop;
	assign wData  	= 	(active) ? sramWriteData : procWriteData;
//----------State----------------------------------------------			
	assign procStall = (state[2] & ~statep[2]) | ~state[2];
	assign active = ~state[2] & ~state[0];
	assign activeWrite = active & (hitActive | state[1]);
	assign passiveWrite = (~active) & ~readOrWrite & hitSnoop;
	assign control = {state, currProc == NUM , procRe , procWe , hitActive };
	always @(*) 
		casez(control)
			7'b100?1?0: statep <= 3'b011;//Nothing to wait for read
			7'b100?01?: statep <= 3'b001;//Nothing to wait for write								
			7'b0111???: statep <= 3'b010;//Wait for read to reading	
			7'b0100???: statep <= 3'b100;//Reading to nothing
			7'b0011???: statep <= 3'b000;//Wait for write to writing
			7'b0000???: statep <= 3'b100;//Writing to nothing
			default:	statep <= state; //Stay on current state
		endcase
//----------Output-SRAM---------------------------------------------	
	assign sramAddress = (active) ? addr : 32'bz;
	assign readOrWrite = (active) ? state[1] : 1'bz;
	assign sramReadData = (active) ? procWriteData : 0;
	assign info = state;
//-------------CACHE----------------------------------------------		
		
	Two_Way_RAM #(32,256,8) 					c1Data	(clk, activeWrite, 						wayActive, 		setActive, 		wData, 	procReadData);
	Two_Way_RAM_Two_Way_Read #(24,256,8) 	c1Tag		(clk, activeWrite, 						wayActive, 		Set, 				wTag, 	rTag);
	Two_Way_RAM_Two_Way_Read #(1,256,8) 	c1Valid	(clk, activeWrite || passiveWrite,	Way, 				Set, 				wValid, 	rValid);
	
endmodule



module SRAMControlUnit(	input 			clk, reset, 
								//Processor(s)------
								input 			readOrWrite,
								input [31:0] 	address,	
								input [31:0] 	inputData,
								input [2:0] 	info1,
								input [2:0] 	info2,
								output reg [1:0] 	currProc,
								output [31:0] outputData,
								//SRAM-----------
								input 			SRAMstall,
								input [31:0] 	SRAMreadData,
								output 			SRAMwriteEnable,
								output 			SRAMreadEnable,
								output [16:0] 	SRAMAddress,
								output [31:0] 	SRAMwriteData
								);		
	reg [1:0] nextProc;
	wire [4:0] control;
	wire active;
	initial begin
		currProc <= 2'b00;
		nextProc <= 2'b00;
	end 
	always @(posedge clk or posedge reset) begin
		if(reset) currProc <= 2'b00;
		else currProc <= nextProc;
	end
	assign control = {currProc, info1[0] , info2[0] , SRAMstall};
	always @(*) 
		casez(control)
			5'b001??: nextProc <= 2'b01;//Nothing to cache 1
			5'b0001?: nextProc <= 2'b10;//Nothing to cache 2
			5'b01??0: nextProc <= 2'b00;//cache 1 to Nothing
			5'b10??0: nextProc <= 2'b00;//cache 2 to Nothing
			default:	nextProc <= currProc; //Stay on current state
		endcase
	assign active = (currProc[0] | currProc[1]);
	assign SRAMAddress = address[16:0];
	assign SRAMwriteData = inputData;
	assign outputData = SRAMreadData;
	assign SRAMwriteEnable = (~readOrWrite) & active;
	assign SRAMreadEnable = (readOrWrite) & active;
		
endmodule 
