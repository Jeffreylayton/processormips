//------------------------------------------------
// mipsparts.v
// David_Harris@hmc.edu 23 October 2005
// Components used in MIPS processor
//------------------------------------------------

module alu(input      [31:0] a, b, 
           input      [3:0]  alucont, 
           output  [31:0] result,
           output            zero,
			  output  [31:0] hi, lo);

  wire [31:0] AND, OR, ADD, XOR, NOR, SUB, SLT, SLLv, SRLv, SLTU;
  wire [63:0] MULT, MULTU, DIV, DIVU;
  assign AND = a & b;
  assign OR = a | b;
  assign ADD = a + b;
  assign XOR = a ^ b;
  assign NOR = ~OR;
  assign SUB = a + (~b) + 1;
  assign SLT = {31'b0,SUB[31]};
  assign MULT = $signed(a)*$signed(b);
  assign MULTU = a*b;
  assign DIV = 64'b0;//{$signed(a)%$signed(b), $signed(a)/$signed(b)};
  assign DIVU = 64'b0;//{a%b, a/b};
  assign SLLv = a << b[4:0];
  assign SRLv = a >> b[4:0];
  assign SLTU = (a < b); 
  
  reg [95:0] out;
  assign {hi,lo,result} = out;
  
  initial begin
		out <= 96'b0;
	end
  

  always@(*)
    case(alucont[3:0])
		4'b0000: out <= {64'bX, AND}; //AND
		4'b0001:	out <= {64'bX, OR}; //OR
		4'b0010: out <= {64'bX, ADD}; //ADDU
		4'b0011: out <= {64'bX, XOR}; //XOR
		4'b0100: out <= {64'bX, NOR}; //NOR
		4'b0110: out <= {64'bX, SUB};//SUBU		
		4'b0111: out <= {64'bX, SLT};//Slt	
		4'b1000:	out <= {MULT, 32'bX}; //mult
		4'b1001: out <= {MULTU, 32'bX}; //multu		
		4'b1010: out <= {DIV, 32'bX}; //div 
		4'b1011: out <= {DIVU, 32'bX}; //divu		
		4'b1100: out <= {64'bX, SLLv}; //sllv
		4'b1110: out <= {64'bX, SRLv}; //srlv
		4'b1111: out <= {64'bX, SLTU};//Sltu
		default: out <= 96'bX;
    endcase

  assign zero = (result == 32'b0);
endmodule

module regfile(input         clk, 
               input         we3,
					input 		  hle,
               input  [4:0]  ra1, ra2, wa3, 
               input  [31:0] wd3,
					input  [31:0] hi, lo,
               output [31:0] rd1, rd2);
					
					
	wire 	hleR1,hleR2,v1,v2;			

	reg [31:0] rf[31:0];//15-hi 14-lo
	always @(posedge clk) 
			if(we3) rf[wa3] <= wd3;
		
	reg [31:0] hir;
   always @(posedge clk) 
			if(hle) hir <= hi;
			
	reg [31:0] lor;
	always @(posedge clk) 
			if(hle) lor <= lo;

	assign hleR1 	= |(ra1[4:1] ^ 4'b0111);
	assign hleR2 	= |(ra2[4:1] ^ 4'b0111);
	assign v1 		= (ra1[0]) ? hir : lor;
	assign v2 		= (ra2[0]) ? hir : lor;
	assign rd1 		= (hleR1) ? v1 : ((ra1 != 0) ? rf[ra1] : 0);
	assign rd2 		= (hleR2) ? v2 : ((ra2 != 0) ? rf[ra2] : 0);
endmodule

module adder(input [31:0] a, b,
             output [31:0] y);

  assign y = a + b;
endmodule

module sl2 (input  [31:0] a,
           output [31:0] y);

  // shift left by 2
  assign y = {a[29:0], 2'b00};
endmodule

module signext(input  [15:0] a,
               output [31:0] y);
              
  assign y = {{16{a[15]}}, a};
endmodule

module flopr #(parameter WIDTH = 8)
              (input                  clk, reset,
               input      [WIDTH-1:0] d, 
               output reg [WIDTH-1:0] q);
	initial begin
		q <= 0;
	end
	
  always @(posedge clk, posedge reset)
    if (reset) q <= 0;
    else       q <= d;
endmodule

module flopenr #(parameter WIDTH = 8)
                (input                  clk, reset,
                 input                  en,
                 input      [WIDTH-1:0] d, 
                 output reg [WIDTH-1:0] q);
	initial begin
		q <= 0;
	end
	
  always @(posedge clk, posedge reset)
    if      (reset) q <= 0;
    else if (en)    q <= d;
endmodule

module mux2 #(parameter WIDTH = 8)
             (input  [WIDTH-1:0] d0, d1, 
              input              s, 
              output [WIDTH-1:0] y);

  assign y = s ? d1 : d0; 
endmodule
