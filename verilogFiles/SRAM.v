module SRAM(
	input clock,
	
	//CPU interface
	output cpuStall,
	output sramStallout,
	input cpuDoneFlag,
	input cpuWriteEnable,
	input cpuReadEnable,
	
	input [16:0] cpuAddress,
	input [31:0] cpuWriteData,
	output [31:0] readData,

	//SRAM interface
	output [17:0] SRAM_A,
	inout [15:0] SRAM_D,
	output SRAM_CE_n, SRAM_LB_n, SRAM_UB_n, SRAM_OE_n, SRAM_WE_n,
	
	//Serial interface
	input UART_RX,
	output UART_TX
);
	//Spend 1 cycle accessing the lower 16 bit word and then another for the upper 16 bit word
	wire currentWord;
	wire sramStall;
	reg [1:0] opDelay;
	reg [15:0] readBuffer; //Hold the lower 16 bits while waiting for the upper 16 bits
	reg [15:0] writeBuffer; //Buffer output to solve timing
	
	wire [17:0] sramAddress;
	wire [31:0] sramWriteData;
	wire sramWriteEnable, sramReadEnable;
	
	initial begin
		opDelay <= 0;
		readBuffer <= 0;
		writeBuffer <= 0;
		sA <= 0;
		sWe <= 1;
	end
	
	//3 cycle read and write operations
	//First cycle spent buffering outputs, other two spent reading the lower and upper words
	
	reg[17:0] sA;
	assign SRAM_A = sA;
	reg sWe;
	assign SRAM_WE_n = sWe;
	
	always @(posedge clock) begin
		if (sramWriteEnable | sramReadEnable) begin
			if (opDelay == 2)
				opDelay <= 0;
			else
				opDelay <= opDelay + 2'b1;
		end
		
		if (opDelay == 1)
			readBuffer <= SRAM_D;
		
		sA <= sramAddress;
		writeBuffer <= (currentWord ? sramWriteData[31:16] : sramWriteData[15:0]);
		sWe <= ~ (sramWriteEnable & opDelay != 0);
	end
	
	assign currentWord = (sramReadEnable & opDelay >= 1) | (sramWriteEnable & opDelay >= 2);
	assign sramStall = (sramWriteEnable | sramReadEnable) & opDelay != 2;
	assign cpuStall = sramStall | ~cpuActive;
	assign sramStallout = sramStall;//------------------


	//Static SRAM assignments
	assign SRAM_CE_n = 1'b0; //Enable chip
	assign SRAM_LB_n = 1'b0; //Enable lower byte
	assign SRAM_UB_n = 1'b0; //Enable upper byte
	assign SRAM_OE_n = 1'b0; //Enable output (overridden by WE)
	
	assign SRAM_D = sramWriteEnable ? writeBuffer : 16'bZ;

	assign readData = {SRAM_D, readBuffer};
	
	assign sramAddress = (state == 3) ? {cpuAddress, currentWord} : {serialMemoryAddress, currentWord};
	assign sramWriteData = (state == 3) ? cpuWriteData : serialWriteData;
	assign sramWriteEnable = (state == 3) ? cpuWriteEnable : serialWriteEnable;
	assign sramReadEnable = (state == 3) ? cpuReadEnable : serialReadEnable;
	
	//Serial - SRAM interface
	reg [16:0] serialMemoryAddress;
	reg [31:0] serialWriteData, serialReadData;
	reg serialWriteEnable, serialReadEnable;
	
	//Serial state
	reg [31:0] state;
	reg [1:0] rxByteIndex, txByteIndex;
	reg [31:0] rxWordBuffer, txWordBuffer;
	reg rxWordReady, txWordReady;
	
	reg cpuActive;
	reg [31:0] cpuTimer, cpuMemoryOperationCounter;
	
	initial begin
		serialWriteEnable = 0;
		serialReadEnable = 0;
		
		state = 0;
		rxByteIndex = 0;
		txByteIndex = 0;
		rxWordReady = 0;
		txWordReady = 1;
		serialMemoryAddress = 17'b11111111111111111;
		
		cpuActive = 0;
		cpuTimer = 0;
		cpuMemoryOperationCounter = 0;
	end
	
	always @(posedge clock) begin
		if(~sramStall) begin
			// Transmit word
			if(~txWordReady & txReadyOut) begin
				txByte <= txWordBuffer[31:24];
				txWordBuffer <= {txWordBuffer[23:0], 8'b0};
				txBegin <= 1;
			
				txByteIndex <= txByteIndex + 1'b1;
				if(txByteIndex == 3)
					txWordReady <= 1;
			end else begin
				txBegin <= 0;
			end
			
			// Receive word
			if(rxAvailableOut) begin
				rxWordBuffer <= {rxWordBuffer[23:0], rxByteOut};
				
				rxByteIndex <= rxByteIndex + 1'b1;
				if(rxByteIndex == 3)
					rxWordReady <= 1;
			end
			
			//Clear indicator bits
			if(rxWordReady)
				rxWordReady <= 0;
			
			if(serialWriteEnable)
				serialWriteEnable <= 0;
				
			if(serialReadEnable) begin
				serialReadEnable <= 0;
				serialReadData <= readData; //Need to buffer it because the SRAM controller resets currentWord on the next clock tick
			end
			
			//Handle current state
			if (serialMemoryAddress == 10000) begin
				state <= 0;
				serialMemoryAddress <= 17'b11111111111111111;
			end else if(state == 0 & rxWordReady) begin
				state <= rxWordBuffer;
			end else if(state == 1 & rxWordReady) begin //Write content of SRAM
				serialWriteData <= rxWordBuffer;
				serialWriteEnable <= 1;
				serialMemoryAddress <= serialMemoryAddress + 17'b1;
			end else if(state == 2 & txWordReady) begin //Read content of SRAM
				txWordBuffer <= serialReadData;
				txWordReady <= 0;
				serialReadEnable <= 1;
				serialMemoryAddress <= serialMemoryAddress + 17'b1;
			end else if(state == 4) begin //Output number of cycles elapsed
				txWordBuffer <= cpuTimer;
				txWordReady <= 0;
				state <= 0;
			end else if(state == 5) begin //Output number of cycles that were spent doing memory operations
				txWordBuffer <= cpuMemoryOperationCounter;
				txWordReady <= 0;
				state <= 0;
			end else if(state == 6) begin //Output static value so we can check connection
				txWordBuffer <= 123456789;
				txWordReady <= 0;
				state <= 0;
			end else if(state > 6) begin
				state <= 0;
			end
		end
		
		if(state == 3) begin //Run CPU
			if(cpuDoneFlag) begin
				state <= 0;
				cpuActive <= 0;
			end else begin
				if(cpuWriteEnable | cpuReadEnable) begin
					cpuMemoryOperationCounter <= cpuMemoryOperationCounter + 1;
				end
				
				cpuTimer <= cpuTimer + 1;
				cpuActive <= 1;
			end
		end
	end
	
	assign rxClear = (~sramStall & rxAvailableOut);
	
	reg [7:0] txByte;
	wire [7:0]rxByteOut;
	reg txBegin;
	wire txReadyOut, rxClear, rxAvailableOut;
	
	SerialPort serialPort(clock,
									UART_TX, txByte, txBegin, txReadyOut,
									UART_RX, rxByteOut, rxClear, rxAvailableOut);

endmodule 

module SerialPort(
		input clock,
		
		output txOut, //Serial Tx pin
		input [7:0] txByte, //Byte to transmit
		input txBegin, //Begin transmission
		output txReadyOut, //High when finished transmitting the last byte
		
		input rx, //Serial Rx pin
		output [7:0] rxByteOut, //Received byte
		input rxClear, //Set high to clear rxAvailable
		output rxAvailableOut //High while there is a new byte available
	);
	
	//Serial transmit
	reg tx; //Output bit
	reg [9:0] txDelay; //Delay counter for baud rate
	reg [3:0] txBitIndex; //Current bit in a packet
	reg [7:0] txBuffer; //Output buffer
	reg txReady;
	
	//Serial recieve
	reg [9:0] rxDelay; //Delay counter for baud rate
	reg [3:0] rxBitIndex; //Current bit in a packet
	reg [7:0] rxBuffer; //Byte currently being received
	reg [7:0] rxByte; //Last complete byte received
	reg rxReady;
	reg rxAvailable;
	
	assign txOut = tx;
	assign txReadyOut = txReady && ~txBegin;
	assign rxByteOut = rxByte;
	assign rxAvailableOut = rxAvailable;
	
	initial begin
		tx <= 1;
		txDelay <= 10'b0;
		txBitIndex <= 4'b0;
		txBuffer <= 8'b0;
		txReady <= 1;
		
		rxDelay <= 10'b0;
		rxBitIndex <= 4'b0;
		rxBuffer <= 8'b0;
		rxByte <= 8'b0;
		rxReady <= 1;
		rxAvailable <= 0;
	end
	
	
	always @(posedge clock) begin
		//Serial transmit
		if(txReady && txBegin) begin //Finished the last byte and another byte is available
			txBuffer <= txByte; //Load the byte to be sent into the buffer
			txReady <= 0; //Begin transmission
		end
		
		if(!txReady) begin
			if(txDelay == 194) begin //At 50MHz this delay gives 256000 baud rate
				txDelay <= 10'b0;
				
				txBitIndex <= txBitIndex + 4'b1;
				if(txBitIndex == 0) begin //Start bit
					tx <= 0;
				end else if (txBitIndex == 9) begin //Stop bit
					tx <= 1;
				end else if (txBitIndex == 10) begin //Need to wait the full time for the stop bit, otherwise consequtive characters are lost
					txBitIndex <= 0;
					txReady <= 1;
				end else begin //Data bits
					tx <= txBuffer[0]; //Output current bit
					txBuffer <= {txBuffer[0], txBuffer[7:1]}; //Right circular shift by 1
				end
			end else begin
				txDelay <= txDelay + 10'b1;
			end
		end
		
		
		//Serial receive
		if(rxClear) rxAvailable <= 0; //Clear byte available flag
		
		if(rxReady && !rx) begin //Begin receiving on start bit
			rxReady <= 0;
		end
		
		if(!rxReady) begin //Receiving message
			if(rxDelay == 194) begin
				rxDelay <= 10'b0;
				
				rxBuffer[rxBitIndex] <= rx; //Add next bit into buffer
				rxBitIndex <= rxBitIndex + 4'b1;
			end else begin
				rxDelay <= rxDelay + 10'b1;
			end
		end else begin
			rxDelay <= 10'd950; //Start -97 clock cycles so we sample all the other bits in the middle of their periods
		end
		
		if(rxBitIndex == 9) begin
			rxReady <= 1; //Done receiving
			rxAvailable <= 1; //Byte available
			rxByte <= rxBuffer; //Output received byte
			rxBitIndex <= 4'b0;
		end
	end
endmodule