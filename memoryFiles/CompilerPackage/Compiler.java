package CompilerPackage;

import java.io.*;


public class Compiler{
	
	static final String[] name = {"SLLV","SRLV","MULTU","MULT","DIVU","DIV","ADDU","SUBU","AND","OR","XOR","NOR","SLTU","SLT","LW","SW","BEQ","ADDIU","J"};
	static final String[] opdecode = {"000000","000000","000000","000000","000000","000000","000000","000000","000000","000000","000000","000000","000000","000000","100011","101011","000100","001001","000010"};
	static final String[] funcdecode = {"000100","000110","011001","011000","011011","011010","100001","100011","100100","100101","100110","100111","101011","101010",null,null,null,null,null};

	static final String[] registerAlias = {"ZERO","AT","V0","V1","A0","A1","A2","A3","T0","T1","T2","T3","T4","T5","T6","T7","S0","S1","S2","S3","S4","S5","S6","S7","T8","T9","K0","K1","GP","SP","FP","RA"};
	
	public static void main( String[] arg) {
		if(arg.length != 2){
			System.out.println("Error, not enough inputs");
			return;
		}
		BufferedReader br = null;
		BufferedWriter bw = null;
		try{
			br = new BufferedReader(new FileReader(arg[0]));
			bw = new BufferedWriter(new FileWriter(arg[1]));
			Compiler.convert(br,bw);

		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			if(br != null) br.close();
			if(bw != null) bw.close();
		}catch(Exception e){}
	}
	
/* 		6'b000100:controls <= 5'b11100; // sllv
		6'b000110:controls <= 5'b11110; // srlv
		6'b011000:controls <= 5'b11000; // mult
		6'b011001:controls <= 5'b11001; // multu
		6'b011010:controls <= 5'b11010; // div
		6'b011011:controls <= 5'b11011; // divu
        6'b100001:controls <= 5'b00010; // ADDU
        6'b100011:controls <= 5'b00110; // SUBU
        6'b100100:controls <= 5'b00000; // AND
        6'b100101:controls <= 5'b00001; // OR
        6'b100110:controls <= 5'b00011; // XOR
        6'b100111:controls <= 5'b00100; // NOR
		6'b101011:controls <= 5'b01111; // SLTU
        6'b101010:controls <= 5'b00111; // SLT
		
		6'b000000: controls <= 9'b110000010; //Rtype
		6'b100011: controls <= 9'b101001000; //LW
		6'b101011: controls <= 9'b001010000; //SW
		6'b000100: controls <= 9'b000100001; //BEQ
		6'b001001: controls <= 9'b101000000; //ADDIU
		6'b000010: controls <= 9'b000000100; //J 
		
		*/
	
	public static void convert(BufferedReader br, BufferedWriter bw) throws CompilerException,IOException{
		int i = 0, n = 0; 
		try{
			String inst;
			for(String line = br.readLine(); line != null ; line = br.readLine()){
				inst = convertFirstInstruction(line);
				System.out.println(line + " : " + inst);
				if(inst != null){
					bw.write(inst,0,inst.length());
					bw.newLine();
					n++;
				}
				i++;
			}
			System.out.println("Number of instructions: " + n);
		}catch(CompilerException e){
			throw new CompilerException("Line " + i + ":" + System.getProperty("line.separator"), e);
		}
	}
	
	public static String convertFirstInstruction(String line) throws CompilerException{
			if(line == null){
				return null;
			}
			String temp;
			String op = null, rs = null, rt = null, rd = null, shift = null, func = null, imm = null, address = null ,instruction = null;
			int type;
			temp = line.replace("\t","").replace(" ", "").toUpperCase().replace("0X","0x");			
			op = "000000";
			type = -1;
			int i;
			temp = temp.replace("NOP","ADDU$0,$0,$0");
			for(i = 0 ; i < name.length ; i++){
				if(temp.startsWith(name[i])){
					op = opdecode[i];
					func = funcdecode[i];
					if(func != null) type = 0;
					else if(name[i].equals("J")) type = 2;
					else type = 1;
					temp = temp.replace(name[i],"");
					break;
				}
			}

			Integer tmpInt;
			switch(type){
				case 0:
					shift = "00000";
					if(!name[i].equals("MULT") && !name[i].equals("MULTU") && !name[i].equals("DIV") && !name[i].equals("DIVU")){ 
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rd");
						rd = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get rd");
						temp = temp.substring(1);
					}else rd = "00000";
					tmpInt = grabNextRegister(temp);
					if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rs");
					rs = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
					temp = removeNextRegister(temp);
					if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get rs");
					temp = temp.substring(1);
					tmpInt = grabNextRegister(temp);
					if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rt");
					rt = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
					temp = removeNextRegister(temp);
					if(!temp.startsWith("//") && temp.length() > 0 ) throw new  CompilerException("Error with " + name[i] + " instruction: endline comments");
					instruction = op + rs + rt + rd + shift + func;
				break;					
				case 1: 
					//sw$t,offset($s) 
					//lw$t,offset($s) 
					//beq$s,$t,offset 
					//addiu$t,$s,offset
					if(name[i].equals("SW") || name[i].equals("LW")){
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rt");
						rt = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get offset");
						temp = temp.substring(1);
						tmpInt = grabNextNumber(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: error on offset");
						else if(tmpInt < -32768 || tmpInt > 32767) throw new CompilerException("Error with " + name[i] + " instruction: " + tmpInt + " is invalid input address");
						if(tmpInt >= 0) imm = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-16);//will be positive
						else imm = Integer.toBinaryString(tmpInt).substring(32-16);
						temp = removeNextNumber(temp);
						if(temp.charAt(0) != '(') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get rs");
						temp = temp.substring(1);
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rs");
						rs = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ')') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get rs");
						temp = temp.substring(1);
						if(!temp.startsWith("//") && temp.length() > 0 ) throw new  CompilerException("Error with " + name[i] + " instruction: endline comments");
					}else if (name[i].equals("BEQ")){
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rs");
						rs = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get rt");
						temp = temp.substring(1);
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rt");
						rt = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get offset");
						temp = temp.substring(1);
						tmpInt = grabNextNumber(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get offset");
						else if(tmpInt < -32768 || tmpInt > 32767) throw new CompilerException("Error with " + name[i] + " instruction: " + tmpInt + " is invalid input address");
						if(tmpInt >= 0) imm = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-16);//will be positive
						else imm = Integer.toBinaryString(tmpInt).substring(32-16);
						temp = removeNextNumber(temp);
						if(!temp.startsWith("//") && temp.length() > 0 ) throw new  CompilerException("Error with " + name[i] + " instruction: endline comments");
					}else{
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rt");
						rt = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get rt");
						temp = temp.substring(1);
						tmpInt = grabNextRegister(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get rs");
						rs = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-5);//will be positive
						temp = removeNextRegister(temp);
						if(temp.charAt(0) != ',') throw new  CompilerException("Error with " + name[i] + " instruction: unable to get offset");
						temp = temp.substring(1);
						tmpInt = grabNextNumber(temp);
						if(tmpInt == null) throw new CompilerException("Error with " + name[i] + " instruction: unable to get offset");
						else if(tmpInt < -32768 || tmpInt > 32767) throw new CompilerException("Error with " + name[i] + " instruction: " + tmpInt + " is invalid input address");
						if(tmpInt >= 0) imm = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-16);//will be positive
						else imm = Integer.toBinaryString(tmpInt).substring(32-16);
						temp = removeNextNumber(temp);
						if(!temp.startsWith("//") && temp.length() > 0 ) throw new  CompilerException("Error with " + name[i] + " instruction: endline comments");
					}
					instruction = op + rs + rt + imm;
				break;					
				case 2: 
					tmpInt = grabNextNumber(temp);
					if(tmpInt == null) throw new CompilerException("Error with jump instruction: error on address");
					else if(tmpInt < 0 || tmpInt > 67108863) throw new CompilerException("Error with jump instruction: " + tmpInt + " is invalid input address");
					address = signExtend("0"+Integer.toBinaryString(tmpInt),32).substring(32-26);//will be positive
					temp = removeNextNumber(temp);
					System.out.println(temp);
					if(!temp.startsWith("//") && temp.length() > 0 ) throw new  CompilerException("Error with " + name[i] + " instruction: endline comments");
					instruction = op + address;
				break;
				default:
					instruction = null;
			}
			return binaryToHex(instruction);
	}
	
    public static Integer grabNextNumber(String a){
		if(a == null) return null;
		int ret, sign = 0, numbertype = 0;
		if(a.startsWith("0x")){
			numbertype = 1;
			a = a.substring(2);
		}else if(a.startsWith("-")){
			sign = 1;
			a = a.substring(1);
		}
		
		String num = "";
		while(a != null && a.length() != 0){
			if(!isValidDigit(a.charAt(0),numbertype))
				break;
			num = num + a.charAt(0);
			a = a.substring(1);		
		}
		if(num.length() == 0) return null;
		try{
		    if(numbertype == 1) ret = Integer.parseInt(num, 16);  
		    else ret = Integer.parseInt(num); 
            if(sign == 1) ret = -ret;
		}catch(Exception s){
		    return null;
		}
		return ret;
	}
	
	public static String removeNextNumber(String a){
		if(a == null) return a;
		int ret, sign = 0, numbertype = 0;
		String removed = "";
		if(a.startsWith("0x")){
			numbertype = 1;
			removed = "0x";
			a = a.substring(2);
		}else if(a.startsWith("-")){
		    sign = 0;
			removed = "-";
			a = a.substring(1);
		}
		
		String num = "";
		while(a != null && a.length() != 0){
			if(!isValidDigit(a.charAt(0),numbertype))
				break;
			num = num + a.charAt(0);
			removed = removed + a.charAt(0);
			a = a.substring(1);		
		}
		if(num.length() == 0) return removed + a;
		try{
		    if(numbertype == 1) ret = Integer.parseInt(num, 16);  
		    else ret = Integer.parseInt(num); 
            if(sign == 1) ret = -ret;
		}catch(Exception s){
		    return removed + a;
		}
		return a;
	}
	
	public static boolean isValidDigit(char a){
		return isValidDigit(a,0);
	}
	
	public static boolean isValidDigit(char a, int type){
		if(type == 0){
			if( a >= '0' && a <= '9' ) return true; 
		}else if (type == 1){
			if( (a >= '0' && a <= '9') || (a >= 'a' && a <= 'f') || (a >= 'A' && a <= 'F') ) return true; 			
		}
		return false;
	}
	
	public static Integer grabNextRegister(String a){
		if(a == null) return null;
		Integer ret;
		if(!a.startsWith("$")) return null;
		a = a.substring(1);
		if(Character.isDigit(a.charAt(0))){
		   Integer tmp = grabNextNumber(a);
		   if(tmp >= 0 && tmp <= 31) return tmp;
		}else for(int i = 0 ; i < registerAlias.length ; i++) if(a.startsWith(registerAlias[i])) return i;
		return null;
	}
	
	public static String removeNextRegister(String a){
		if(a == null) return a;
		if(!a.startsWith("$")) return a;
		a = a.substring(1);
		if(Character.isDigit(a.charAt(0))){
		   Integer tmp = grabNextNumber(a);
		   if(tmp >= 0 && tmp <= 9) return a.substring(1);
		   if(tmp >= 10 && tmp <= 31) return a.substring(2);
		}else for(int i = 0 ; i < registerAlias.length ; i++) if(a.startsWith(registerAlias[i])) return a.substring(registerAlias[i].length());
		return "$" + a;
	}
	
	public static String signExtend(String b, int nRadix){
        if(b == null) return null;
        String tmp = b.replace("1","").replace("0","");
        if(tmp.length() != 0 ) return null;
        if(b.length() > nRadix) return null;
        String z = "";
        for(int j = 0 ; j < (nRadix - b.length() ) ; j++)
            z = b.charAt(0) + z;
        return z + b;
    }
    public static String binaryToHex(String b){
		if(b == null) return b;
		if(b.replace("0","").replace("1","").length() != 0) return null;
		String ret = "";
		int r = b.length() % 4;
		if(r > 0){
			String tmp = b.substring(0,r-1);
			while(tmp.length() < 4) tmp = "0" + tmp; 
			ret = ret + binaryToHex4(tmp);
			b = b.substring(r);
		}
		while( b.length() != 0){
			String tmp = b.substring(0,4);
			ret = ret + binaryToHex4(tmp);
			b = b.substring(4);
		}
		return ret;
	}
	 
	private static String binaryToHex4(String b){
		if(b == null) return b;
		if(b.length() != 4) return null;
		switch(b){
			case "0000": 	return "0";
			case "0001": 	return "1";
			case "0010": 	return "2";
			case "0011": 	return "3";
			case "0100": 	return "4";
			case "0101": 	return "5";
			case "0110": 	return "6";
			case "0111": 	return "7";
			case "1000": 	return "8";
			case "1001": 	return "9";
			case "1010": 	return "a";
			case "1011": 	return "b";
			case "1100": 	return "c";
			case "1101": 	return "d";
			case "1110": 	return "e";
			case "1111": 	return "f";
			default: 		return null;
		}
	}
}