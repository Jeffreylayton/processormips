package CompilerPackage;

public class CompilerException extends Exception{
	private static final String info = "This exception is a marker for our mips compiler.";

	public static String Info(){
		return info;
	}
	
	public CompilerException(String r){
		super(r);
	}
	public CompilerException(String r, Exception e){
		super(r,e);
	}
}